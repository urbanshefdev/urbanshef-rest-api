# Slobite REST API

The application contains source code for:

 - Chef authentication
 - Chef dashboard
 - Urbanshef administration
 - API endpoints
 - Transactional email
 - Transactional SMS
 - Order management
 - Meal management
 - Review management

## Python & Django version

- Python 2.7.16
- Django 3.1.7

## Getting started

To start project, run:

```
docker-compose up
```

